<?php

namespace KDA\JSON;

use Balping\JsonRaw\Raw;
use Balping\JsonRaw\Encoder;


class Json {


    public function raw ($string){
        return new Raw($string);
    }

    public function encode($array){
        return Encoder::encode($array);
    }
    public function decode ($array,$associative=true,$depth=512,$flags=0){
        return json_decode($array,$associative,$depth,$flags);
    }
}