<?php

namespace KDA\JSON;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\HasHelper;

    public function register()
    {

        parent::register();

        $this->app->singleton('json',function($app){
            return new Json();
        });
    }
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
